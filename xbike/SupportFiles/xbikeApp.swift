//
//  xbikeApp.swift
//  xbike
//
//  Created by Daniel Orellana on 5/07/22.
//

import SwiftUI

@main
struct xbikeApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    let persistenceController = PersistenceController.shared
    
    @StateObject var locationManager = LocationManager()

    var body: some Scene {
        WindowGroup {
            MainOnboardingView()
                .environmentObject(locationManager)
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
