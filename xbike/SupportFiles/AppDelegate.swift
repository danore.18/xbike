//
//  AppDelegate.swift
//  xbike
//
//  Created by Daniel Orellana on 5/07/22.
//

import Foundation
import UIKit
import GoogleMaps

class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        GMSServices.provideAPIKey("AIzaSyBKemtmwFhhCzL6FBYsrD4kbSv_qbjpUu0")
        return true
    }
}
