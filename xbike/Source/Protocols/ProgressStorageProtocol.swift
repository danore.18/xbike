//
//  ProgressStorageProtocol.swift
//  xbike
//
//  Created by Daniel Orellana on 6/07/22.
//

import Foundation

protocol ProgressStorageProtocol {
    func save(_ item: ProgressModel?)
    func get()
}
