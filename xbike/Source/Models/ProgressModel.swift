//
//  ProgressModel.swift
//  xbike
//
//  Created by Daniel Orellana on 6/07/22.
//

import Foundation

struct ProgressModel {
    
    var time: String
    var distance: String
    var address: String
    
}
