//
//  Navigation+Helper.swift
//  xbike
//
//  Created by Daniel Orellana on 6/07/22.
//

import SwiftUI

extension View {
    
    func orangeNavigationBar() -> some View {
        modifier(NavigationBarViewModifier())
    }
}

struct NavigationBarViewModifier: ViewModifier {

    func body(content: Content) -> some View {
        content
            .onAppear {
                NavigationBarTheme.blueNavigationBar()
            }
    }
}

class NavigationBarTheme {

    static func blueNavigationBar() {
        UINavigationBar.appearance().tintColor = UIColor.white

        let appearance = UINavigationBarAppearance()
        appearance.backgroundColor = UIColor(Color.orangeApp)
        appearance.titleTextAttributes = [
            .foregroundColor: UIColor(Color.white),
            .textEffect: NSAttributedString.TextEffectStyle.letterpressStyle
        ]

        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        UINavigationBar.appearance().compactAppearance = appearance
    }
}
