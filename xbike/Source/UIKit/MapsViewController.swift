//
//  MapsViewController.swift
//  xbike
//
//  Created by Daniel Orellana on 5/07/22.
//

import Foundation
import UIKit
import GoogleMaps
import Combine
import SwiftUI

class MapsViewController: UIViewController {
    private var cancellables = [AnyCancellable]()
    @StateObject private var location = LocationManager()
    
    override func viewDidLoad() {
        self.observers()
        
        let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
               let mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
               self.view.addSubview(mapView)

               // Creates a marker in the center of the map.
               let marker = GMSMarker()
               marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
               marker.title = "Sydney"
               marker.snippet = "Australia"
               marker.map = mapView
    }
    
    private func observers() {
        location.$lastLocation
            .receive(on: DispatchQueue.main)
            .sink { error in
                print(error)
            } receiveValue: { location in
                print("LOCATION TEST -> \(String(describing: location?.coordinate.longitude))")
                print("LOCATION TEST -> \(String(describing: location?.coordinate.latitude))")
            }
            .store(in: &cancellables)
    }
    
}
