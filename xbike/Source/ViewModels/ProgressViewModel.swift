//
//  ProgressViewModel.swift
//  xbike
//
//  Created by Daniel Orellana on 6/07/22.
//

import Foundation
import SwiftUI
import Combine

class ProgressViewModel: ObservableObject {
    @ObservedObject var storage: ProgressStorage = ProgressStorage()
    
    @Published var items: [Progress] = []
    @Published var isSuccess: Bool = false
    
    private var cancellables = [AnyCancellable]()
    
    init() {
        storage.$items
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] data in
                if data.count > 0 {
                    self?.items = data
                }
            })
            .store(in: &cancellables)
        
        storage.$isProgressSaved
            .receive(on: DispatchQueue.main)
            .sink { [weak self] success in
                self?.isSuccess = success
            }
            .store(in: &cancellables)
    }
    
    func getData() {
        storage.get()
    }
    
    func saveData(_ item: ProgressModel) {
        storage.save(item)
    }
    
}
