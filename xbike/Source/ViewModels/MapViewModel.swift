//
//  MapViewModel.swift
//  xbike
//
//  Created by Daniel Orellana on 6/07/22.
//

import Foundation
import SwiftUI
import GoogleMaps

class MapViewModel: ObservableObject {
    
    @Published var coordinates: CLLocationCoordinate2D = CLLocationCoordinate2D()
    @Published var coordinatesArray: [CLLocationCoordinate2D] = []
    @Published var isTracking: Bool = false
    @Published var showStartRide: Bool = false
    @Published var readyToSave: Bool = false
    @Published var isRideSaved: Bool = false
    @Published var currentTime: String = "00:00:00"
    @Published var distance: String = "0.0 km"
    @Published var address: String = ""
    
    lazy var item: ProgressModel = {
        return ProgressModel(time: currentTime, distance: distance, address: address)
    }()
    
    private var timer = Timer()
    private var seconds = 0
    
    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
    }
    
    func stopTimer() {
        getAddress()
        timer.invalidate()
    }
        
    func initDefaultData() {
        currentTime = "00:00:00"
        distance = "0.0 km"
        address = ""
        seconds = 0
    }
    
    @objc private func updateTimer() {
        seconds += 1
        currentTime = timeString(time: TimeInterval(seconds))
    }
    
    private func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
        
    func getAddress() {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinates) { response, error in
            if error != nil {
                print("reverse geodcode fail: \(error!.localizedDescription)")
            } else {
                if let places = response?.results() {
                    if let place = places.first {
                        if let lines = place.lines {
                            print("GEOCODE: Formatted Address: \(lines)")
                            self.address = lines.joined(separator: ",")
                        }
                    } else {
                        print("GEOCODE: nil first in places")
                    }
                } else {
                    print("GEOCODE: nil in places")
                }
            }
        }
    }
    
}
