//
//  ProgressStorage.swift
//  xbike
//
//  Created by Daniel Orellana on 6/07/22.
//

import Foundation
import CoreData
import SwiftUI

class ProgressStorage: ObservableObject, ProgressStorageProtocol {
    
    private var context: NSManagedObjectContext
    
    @Published var items: [Progress] = []
    @Published var fetchRemoteData: Bool = false
    @Published var isProgressSaved: Bool = false
    
    init() {
        self.context = PersistenceController.shared.container.viewContext
    }
    
    func save(_ item: ProgressModel?) {
        guard let progress = item else {
            print("Error")
            return
        }
        
        let storage = Progress(context: context)
        storage.id = UUID()
        storage.time = progress.time
        storage.distance = progress.distance
        storage.address = progress.address
        
        do {
            try context.save()
            isProgressSaved = true
        } catch let error as NSError {
            print("DBError: \(error.localizedDescription)")
            isProgressSaved = false
        }
    }
    
    func get() {
        let request: NSFetchRequest<Progress> = Progress.fetchRequest()
        
        do {
            let data = try context.fetch(request)
            
            if data.count > 0 { items = data }
            else { fetchRemoteData = true }
        } catch let error as NSError {
            print("DBError: \(error.localizedDescription)")
        }
    }
    
}
