//
//  ListView.swift
//  xbike
//
//  Created by Daniel Orellana on 5/07/22.
//

import SwiftUI

struct ListView: View {
    @StateObject var progressViewModel = ProgressViewModel()
    
    var body: some View {
        NavigationView {
            VStack {
                if !progressViewModel.items.isEmpty {
                    List(progressViewModel.items) { item in
                        HStack {
                            VStack(alignment: .leading) {
                                Text(item.time ?? "Not available")
                                    .font(.system(size: 25))
                                Text(item.address ?? "Not available")
                                    .font(.system(size: 12))
                                    .padding(.trailing, 15)
                            }
                            
                            Spacer()
                            
                            Text(item.distance ?? "N/A")
                                .font(.system(size: 25))
                        }
                        .padding()
                    }
                    .listStyle(PlainListStyle())
                } else {
                    Text("No results yet")
                }
            }
            .onAppear(perform: {
                progressViewModel.getData()
            })
            .navigationTitle("My Progress")
            .orangeNavigationBar()
            .navigationBarTitleDisplayMode(.inline)
        }
    }
}

struct ListView_Previews: PreviewProvider {
    static var previews: some View {
        ListView()
    }
}
