//
//  TabsView.swift
//  xbike
//
//  Created by Daniel Orellana on 5/07/22.
//

import SwiftUI

struct TabsView: View {
    @EnvironmentObject var location: LocationManager
    
    var body: some View {
        VStack {
            TabView {
                MapView()
                    .environmentObject(location)
                    .tabItem {
                        Label("Current ride", systemImage: "map")
                    }

                ListView()
                    .tabItem {
                        Label("My progress", systemImage: "list.bullet.rectangle.portrait")
                    }
            }
            .tabViewStyle(.automatic)
            .tint(.orangeApp)
            .accentColor(.orangeApp)
        }
    }
}
