//
//  GoogleMapsView.swift
//  xbike
//
//  Created by Daniel Orellana on 5/07/22.
//

import Foundation
import SwiftUI
import GoogleMaps
import Combine
import MapKit

struct GoogleMapsView: UIViewRepresentable {
    @Binding var coordinates: CLLocationCoordinate2D
    @Binding var coordinatesArray: [CLLocationCoordinate2D]
    @Binding var isTracking: Bool
    @Binding var distance: String
    
    func makeUIView(context: Context) -> GMSMapView {
        let mapView = GMSMapView(frame: CGRect.zero)
        mapView.isMyLocationEnabled = true
        
        DispatchQueue.main.async {
            mapView.camera = GMSCameraPosition.camera(withLatitude: coordinates.latitude, longitude: coordinates.longitude, zoom: 15)
            
            let position = CLLocationCoordinate2D(latitude: coordinates.latitude, longitude: coordinates.longitude)
            let marker = GMSMarker(position: position)
            marker.icon = UIImage(systemName: "circle.fill")
            
            marker.map = mapView
        }
        
        return mapView
    }
    
    func updateUIView(_ uiView: GMSMapView, context: Context) {
        if isTracking {
            DispatchQueue.main.async {
                if coordinatesArray.isEmpty { return }
                
                let path = GMSMutablePath()
                for coord in coordinatesArray {
                    path.add(coord)
                }
                
                let line = GMSPolyline(path: path)
                line.strokeColor = UIColor.orange
                line.strokeWidth = 4.0
                line.map = uiView
                
                distance = "\(String(format: "%.2f", path.length(of: .rhumb).inKilometers())) km"
            }
        } else {
            uiView.clear()
            
            let position = CLLocationCoordinate2D(latitude: coordinates.latitude, longitude: coordinates.longitude)
            let marker = GMSMarker(position: position)
            marker.icon = UIImage(systemName: "circle.fill")
            
            marker.map = uiView
        }
    }
}

extension CLLocationDistance {
    func inKilometers() -> CLLocationDistance {
        return self/1000
    }
}
