//
//  MapView.swift
//  xbike
//
//  Created by Daniel Orellana on 5/07/22.
//

import SwiftUI
import GoogleMaps

struct MapView: View {
    @EnvironmentObject var location: LocationManager
    @StateObject var mapViewModel = MapViewModel()
    @StateObject var progressViewModel = ProgressViewModel()
    
    @State private var showLocationAlert = false
        
    var body: some View {
        NavigationView {
            ZStack {
                GoogleMapsView(
                    coordinates: $mapViewModel.coordinates,
                    coordinatesArray: $mapViewModel.coordinatesArray,
                    isTracking: $mapViewModel.isTracking,
                    distance: $mapViewModel.distance)
                .onReceive(location.$lastLocation, perform: { location in
                    guard let coordinates = location?.coordinate else { return }
                    mapViewModel.coordinates = coordinates
                    
                    if mapViewModel.isTracking {
                        mapViewModel.coordinatesArray.append(coordinates)
                    }
                })
                
                if mapViewModel.showStartRide {
                    trackingTimeView
                }
                
                if mapViewModel.readyToSave {
                    storeView
                }
                
                if mapViewModel.isRideSaved {
                    successView
                }
            }
            .alert("Location services aren't available", isPresented: $showLocationAlert) {
                Button("OK", role: .cancel) { }
            }
            .navigationTitle("Current Ride")
            .orangeNavigationBar()
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem {
                    Button {
                        if location.locationStatus != .authorizedAlways
                            || location.locationStatus != .authorizedWhenInUse {
                            showLocationAlert = true
                            return
                        }
                        mapViewModel.initDefaultData()
                        mapViewModel.showStartRide = true
                    } label: {
                        Image(systemName: "plus")
                            .foregroundColor(.white)
                    }
                    
                }
            }
        }
    }
    
    //MARK: Tracking time view
    var trackingTimeView: some View {
        VStack {
            ZStack {
                RoundedRectangle(cornerRadius: 15)
                    .fill(Color.white)
                    .shadow(color: .gray, radius: 2, x: 0, y: 2)
                VStack(spacing: 40) {
                    Text(mapViewModel.currentTime)
                        .font(.system(size: 30))
                    HStack(spacing: 50) {
                        Button {
                            mapViewModel.startTimer()
                            mapViewModel.isTracking = true
                        } label: {
                            Text("START")
                                .foregroundColor(.orangeApp)
                                .font(.system(size: 14))
                        }
                        .disabled(mapViewModel.isTracking)
                        
                        ZStack {
                            Color.orange
                        }
                        .frame(width: 2, height: 40, alignment: .center)
                        
                        Button {
                            mapViewModel.stopTimer()
                            mapViewModel.isTracking = false
                            mapViewModel.showStartRide = false
                            mapViewModel.readyToSave = true
                        } label: {
                            Text("STOP")
                                .foregroundColor(.gray)
                                .font(.system(size: 14))
                        }
                        .disabled(!mapViewModel.isTracking)
                    }
                }
            }
            .frame(height: 160, alignment: .bottom)
            .padding(.trailing, 25)
            .padding(.leading, 25)
            .padding(.bottom, 25)
        }
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity, alignment: .bottom)
    }
    
    //MARK: Store tracking view
    var storeView: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 15)
                .fill(Color.white)
                .shadow(color: .gray, radius: 2, x: 0, y: 2)
            VStack(spacing: 10) {
                Text("Your time was")
                    .font(.system(size: 18))
                
                Text(mapViewModel.currentTime)
                    .font(.system(size: 30))
                
                Text("Distance")
                    .font(.system(size: 18))
                
                Text(mapViewModel.distance)
                    .font(.system(size: 30))
                
                HStack(spacing: 50) {
                    Button {
                        progressViewModel.saveData(mapViewModel.item)
                    } label: {
                        Text("STORE")
                            .foregroundColor(.orangeApp)
                            .font(.system(size: 14))
                    }
                    
                    ZStack {
                        Color.orange
                    }
                    .frame(width: 2, height: 40, alignment: .center)
                    
                    Button {
                        mapViewModel.readyToSave = false
                        mapViewModel.isTracking = false
                    } label: {
                        Text("DELETE")
                            .foregroundColor(.gray)
                            .font(.system(size: 14))
                    }
                }
            }
        }
        .onReceive(progressViewModel.$isSuccess, perform: { success in
            if success { mapViewModel.readyToSave = false }
            mapViewModel.isRideSaved = success
        })
        .frame(height: 240, alignment: .center)
        .padding(.trailing, 25)
        .padding(.leading, 25)
        .padding(.bottom, 25)
    }
    
    //MARK: Success view
    var successView: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 15)
                .fill(Color.white)
                .shadow(color: .gray, radius: 2, x: 0, y: 2)
            VStack(spacing: 35) {
                Text("Your progress has been correctly stored!")
                    .multilineTextAlignment(.center)
                    .font(.system(size: 18))
                    .padding(.trailing, 25)
                    .padding(.leading, 25)
                
                Button {
                    mapViewModel.isRideSaved = false
                    mapViewModel.distance = ""
                    mapViewModel.address = ""
                    mapViewModel.currentTime = ""
                } label: {
                    Text("OK")
                        .foregroundColor(.gray)
                        .font(.system(size: 14))
                }
            }
        }
        .frame(height: 240, alignment: .center)
        .padding(.trailing, 25)
        .padding(.leading, 25)
        .padding(.bottom, 25)
    }
}
