//
//  MainOnboardingView.swift
//  xbike
//
//  Created by Daniel Orellana on 5/07/22.
//

import SwiftUI

struct MainOnboardingView: View {
    @EnvironmentObject var locationManager: LocationManager
    
    var body: some View {
        TabView {
            FirstStepView()
            SecondStepView()
            ThirdStepView()
                .environmentObject(locationManager)
        }
        .ignoresSafeArea()
        .tabViewStyle(PageTabViewStyle())
    }
}

