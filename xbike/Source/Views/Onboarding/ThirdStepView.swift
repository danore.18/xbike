//
//  ThirdStepView.swift
//  xbike
//
//  Created by Daniel Orellana on 5/07/22.
//

import SwiftUI

struct ThirdStepView: View {
    @EnvironmentObject var locationManager: LocationManager
    @State private var isPresented: Bool = false
    
    var body: some View {
        ZStack {
            Color.orangeApp
                .ignoresSafeArea()
            
            VStack {
                Button {
                    isPresented = true
                } label: {
                    Text("Done")
                        .font(.system(size: 16))
                        .foregroundColor(.white)
                }
                .padding(.trailing, 15)
                .fullScreenCover(isPresented: $isPresented) {
                    TabsView()
                        .environmentObject(locationManager)
                }
            }
            .frame(
                minWidth: 0,
                maxWidth: .infinity,
                minHeight: 0,
                maxHeight: .infinity,
                alignment: .topTrailing)
            
            VStack {
                ZStack {
                    Color.white
                }
                .frame(width: 100, height: 100, alignment: .center)
                
                Text("See your progress and challenge yourself!")
                    .foregroundColor(.white)
                    .font(.system(size: 22))
                    .multilineTextAlignment(.center)
            }
            .padding(.trailing, 100)
            .padding(.leading, 100)
        }
    }
}

struct ThirdStepView_Previews: PreviewProvider {
    static var previews: some View {
        ThirdStepView()
    }
}
