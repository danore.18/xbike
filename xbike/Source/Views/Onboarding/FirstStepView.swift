//
//  FirstStepView.swift
//  xbike
//
//  Created by Daniel Orellana on 5/07/22.
//

import SwiftUI

struct FirstStepView: View {
    var body: some View {
        ZStack {
            Color.orangeApp
                .ignoresSafeArea()
            
            VStack {
                ZStack {
                    Color.white
                }
                .frame(width: 100, height: 100, alignment: .center)
                
                Text("Extremely simple to use")
                    .foregroundColor(.white)
                    .font(.system(size: 22))
                    .multilineTextAlignment(.center)
            }
            .padding(.trailing, 85)
            .padding(.leading, 85)
        }
    }
}

struct FirstStepView_Previews: PreviewProvider {
    static var previews: some View {
        FirstStepView()
    }
}
