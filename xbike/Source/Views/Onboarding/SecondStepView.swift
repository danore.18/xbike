//
//  SecondStepView.swift
//  xbike
//
//  Created by Daniel Orellana on 5/07/22.
//

import SwiftUI

struct SecondStepView: View {
    var body: some View {
        ZStack {
            Color.orangeApp
                .ignoresSafeArea()
            
            VStack {
                
                ZStack {
                    Color.white
                }
                .frame(width: 100, height: 100, alignment: .center)
                
                Text("Track your time and distance")
                    .foregroundColor(.white)
                    .font(.system(size: 22))
                    .multilineTextAlignment(.center)
            }
            .frame(alignment: .center)
            .padding(.trailing, 60)
            .padding(.leading, 60)
        }
    }
}

struct SecondStepView_Previews: PreviewProvider {
    static var previews: some View {
        SecondStepView()
    }
}
